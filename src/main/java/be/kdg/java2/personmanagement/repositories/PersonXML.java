package be.kdg.java2.personmanagement.repositories;

import be.kdg.java2.personmanagement.domain.Person;
import be.kdg.java2.personmanagement.domain.Persons;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PersonXML {
    public void generateXML(Person person, String fileName) {
        try {
            JAXBContext context = JAXBContext.newInstance(Person.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(person, new File(fileName));
            System.out.println("File created");
        } catch (JAXBException jaxbException) {
            jaxbException.printStackTrace();
        }
    }

    public List<Person> loadPersons(String filename) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Persons.class);
            Unmarshaller u = jc.createUnmarshaller();
            File f = new File(filename);
            Persons persons = (Persons) u.unmarshal(f);
            System.out.println(persons.getPersons().size() + " persons loaded...");
            return persons.getPersons();
        } catch (JAXBException jaxbException) {
            jaxbException.printStackTrace();
        }
        return new ArrayList<>();
    }

    public void generateXML(List<Person> persons) {

    }
}
