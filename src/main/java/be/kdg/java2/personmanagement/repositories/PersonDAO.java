package be.kdg.java2.personmanagement.repositories;

import be.kdg.java2.personmanagement.domain.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonDAO {

    public PersonDAO() {
        try {
            Connection connection = createConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("DROP TABLE IF EXISTS PERSONS");
            statement.executeUpdate("CREATE TABLE PERSONS(ID INTEGER NOT NULL IDENTITY, " +
                    "NAME VARCHAR(100) NOT NULL, FIRSTNAME VARCHAR(100) NOT NULL)");
           // statement.executeUpdate("INSERT INTO PERSONS(NAME, FIRSTNAME) " +
           //         "VALUES ('JONES', 'JACK'), ('POTTER', 'MIA'), ('REED', 'JACK'), ('POTTER', 'HARRY')");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadData(PersonXML personXML){
        personXML.loadPersons("persons.xml").forEach(this::create);
    }

    public void loadData(PersonJSON personJSON){
        personJSON.loadPersons("persons.json").forEach(this::create);
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:file:dbData/demo","sa","");
    }

    public List<Person> finddByName(String name) {
        List<Person> persons = new ArrayList<>();
        try {
            Connection connection = createConnection();
            //Statement statement = connection.createStatement();
            PreparedStatement statement
                    = connection.prepareStatement("SELECT * FROM PERSONS WHERE NAME = ?");
            //ResultSet resultSet =
            //        statement.executeQuery("SELECT * FROM PERSONS WHERE NAME = '" + name + "'");
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String personName = resultSet.getString("NAME");
                String personFirstName = resultSet.getString("FIRSTNAME");
                Person person = new Person(resultSet.getInt("ID"), personName, personFirstName);
                persons.add(person);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persons;
    }

    public Person create(Person person) {
        try {
            Connection connection = createConnection();
            PreparedStatement statement
                    = connection.prepareStatement("INSERT INTO PERSONS(NAME, FIRSTNAME) " +
                    "VALUES (?,?)");
            statement.setString(1, person.getName());
            statement.setString(2, person.getFirstName());
            statement.executeUpdate();
            statement.close();
            Statement statement1 = connection.createStatement();
            ResultSet resultSet = statement1.executeQuery("CALL IDENTITY()");
            while (resultSet.next()) {
                person.setId(resultSet.getInt(1));
            }
            resultSet.close();
            statement1.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    public Person findById(int id) {
        Person person = new Person();
        try {
            Connection connection = createConnection();
            PreparedStatement statement
                    = connection.prepareStatement("SELECT * FROM PERSONS WHERE ID = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                person.setId(resultSet.getInt("ID"));
                person.setName(resultSet.getString("NAME"));
                person.setFirstName(resultSet.getString("FIRSTNAME"));
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    public List<Person> findAll() {
        List<Person> persons = new ArrayList<>();
        try {
            Connection connection = createConnection();
            PreparedStatement statement
                    = connection.prepareStatement("SELECT * FROM PERSONS");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String personName = resultSet.getString("NAME");
                String personFirstName = resultSet.getString("FIRSTNAME");
                Person person = new Person(resultSet.getInt("ID"), personName, personFirstName);
                persons.add(person);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persons;
    }
}
