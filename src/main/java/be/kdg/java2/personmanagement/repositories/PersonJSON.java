package be.kdg.java2.personmanagement.repositories;

import be.kdg.java2.personmanagement.domain.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonJSON {

    public List<Person> loadPersons(String filename) {
        try (BufferedReader data = new BufferedReader(
                new FileReader(filename))) {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            Person[] personArray = gson.fromJson(data, Person[].class);
            return Arrays.asList(personArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public void generateJSON(List<Person> persons) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String json = gson.toJson(persons);
        System.out.println(json);
        try (FileWriter jsonWriter = new FileWriter("personsgen.json")) {
            jsonWriter.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
