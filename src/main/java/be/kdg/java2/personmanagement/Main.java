package be.kdg.java2.personmanagement;

import be.kdg.java2.personmanagement.presentation.Menu;
import be.kdg.java2.personmanagement.repositories.PersonDAO;
import be.kdg.java2.personmanagement.repositories.PersonJSON;
import be.kdg.java2.personmanagement.repositories.PersonXML;

public class Main {
    public static void main(String[] args) {
        PersonDAO personDAO = new PersonDAO();
        PersonXML personXML = new PersonXML();
        PersonJSON personJSON = new PersonJSON();
        personDAO.loadData(personJSON);
        Menu menu = new Menu(personDAO, personXML, personJSON);
        menu.run();
    }
}
