package be.kdg.java2.personmanagement.presentation;

import be.kdg.java2.personmanagement.domain.Person;
import be.kdg.java2.personmanagement.repositories.PersonDAO;
import be.kdg.java2.personmanagement.repositories.PersonJSON;
import be.kdg.java2.personmanagement.repositories.PersonXML;

import java.util.List;
import java.util.Scanner;

public class Menu {
    private PersonDAO personDAO;
    private PersonXML personXML;
    private PersonJSON personJSON;
    private Scanner scanner = new Scanner(System.in);

    public Menu(PersonDAO personDAO, PersonXML personXML, PersonJSON personJSON) {
        this.personDAO = personDAO;
        this.personXML = personXML;
        this.personJSON = personJSON;
    }

    public void run(){
        while (true) {
            System.out.println("Welcome to Person Management!");
            System.out.println("Make a choice:");
            System.out.println("1) look up person by name");
            System.out.println("2) add a person");
            System.out.println("3) export a person to XML");
            System.out.println("4) export all persons to JSON");
            System.out.println("5) exit");
            System.out.print("Choice:");
            int choice = scanner.nextInt();
            switch (choice){
                case 1: lookupByName();break;
                case 2: addPerson();break;
                case 3: exportPersonToXML();break;
                case 4: exportAllPersonsToJSON();break;
                case 5: System.exit(0);
            }
        }
    }

    private void exportAllPersonsToJSON() {
        List<Person> persons = personDAO.findAll();
        personJSON.generateJSON(persons);
    }

    private void exportPersonToXML() {
        System.out.print("Enter the ID:");
        int id = scanner.nextInt();
        Person person = personDAO.findById(id);
        personXML.generateXML(person, "person.xml");
    }

    private void addPerson() {
        scanner.nextLine(); //to empty the buffer
        System.out.print("Enter the name:");
        String name = scanner.nextLine();
        System.out.print("Enter the firstname:");
        String firstname = scanner.nextLine();
        Person result = personDAO.create(new Person(name, firstname));
        System.out.println("Entered person with id " + result.getId());
    }

    private void lookupByName() {
        scanner.nextLine(); //to empty the buffer
        System.out.print("Enter the name:");
        String name = scanner.nextLine();
        List<Person> persons = personDAO.finddByName(name);
        persons.forEach(System.out::println);
    }
}
